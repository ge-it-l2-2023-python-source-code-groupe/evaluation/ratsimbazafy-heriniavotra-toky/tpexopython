from core.settings import YOUR_ENV_VARIABLE

def affichage_interpreteur():
    print(f"Sur interpreteur python\n>>>1+1\n{1+1}")
    1+1
    print(f"\nSur le script, 1+1 n'affiche rien car dans le script il faut utiliser la fonction print() de python pour l'afficher  ")

def Poly_A():
    print("A"*20)

def Poly_A_GC():
    print("A"*20+"GC"*40)

def Ecriture_Formate():
    a="salut"
    b=102
    c=10.318
    print(f"{a} {b} {c:.2f}")

def Ecriture_Formate_2():
    perc_GC=((4500 + 2575)/14800)*100
    print(f"le pourcentage de GC est {perc_GC:.0f}% \n le pourcentage de GC est {perc_GC:.1f}% \n le pourcentage de GC est {perc_GC:.2f}% \n")

    