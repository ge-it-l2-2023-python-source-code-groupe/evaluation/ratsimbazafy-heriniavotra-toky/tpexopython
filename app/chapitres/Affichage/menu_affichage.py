from chapitres.Affichage.exo3_6 import*
from tools.console import clear

def Menu_Affichage():
    print("1-Affichage dans l'interpreteur")
    print("2-Poly-A ")
    print("3-Poly-A et Poly-GC")
    print("4-Ecriture formaté")
    print("5-Ecriture formaté 2")
    print("6-Quitter")
    
def Continu():
    continue_encore = input("\nAppuyer sur la touche "'ENTRER'" pour quitter le programme :\n ").lower()
    if continue_encore == ' ':
        return False
    return True

def Affichage():
    while True:
        Menu_Affichage()

        choix_input = input("Entrez votre choix : ")

        if not choix_input:
            clear()
            print("Veuillez entrer quelque chose.")
            continue

        if not choix_input.isdigit():
            clear()
            print("Veuillez entrer un numéro valide.")
            continue

        choix = int(choix_input)

        if choix == 1:
            while True:
                clear()
                print("\n------ Affichage sur interpreteur et dans un programme ------")
                affichage_interpreteur()
                if Continu():
                    break
                
        elif choix == 2:
            clear()
            while True:
                print("\n------ Poly-A------")
                Poly_A()
                if Continu():
                    break
                
        elif choix == 3:
            clear()
            while True:
                print("\n------ Poly-A et Poly- GC ------")
                Poly_A_GC()
                if Continu():
                    break
                
        elif choix == 4:
            clear()
            while True:
                print("\n------ Poly-A et Poly- GC ------")
                Ecriture_Formate()
                if Continu():
                    break
                
        elif choix == 5:
            clear()
            while True:
                print("\n------ Poly-A et Poly- GC ------")
                Ecriture_Formate_2()
                if Continu():
                    break
                
        elif choix == 6:
            clear()
            print("\n------ Au revoir! ------")
            break
        
        else:
            clear()
            print("Choix invalide. Veuillez entrer un numéro valide.")
    clear()
