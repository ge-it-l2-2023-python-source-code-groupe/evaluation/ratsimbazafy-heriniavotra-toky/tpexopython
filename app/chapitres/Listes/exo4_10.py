from core.settings import YOUR_ENV_VARIABLE


def exo1():
    jours = ["lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi", "dimanche"]

    print("1- Les 5 premiers jours de la semaine et week-end")
    liste= jours[:5]
    jour_weekend= jours[5:7]
    print(liste)
    print(jour_weekend)

    print("\n2- Autre façon pour proceder la question n°1")
    autreJour=jours[:-2]
    autreJour_weekend=jours[-2:]
    print(autreJour)
    print(autreJour_weekend)


    print("\n3- Deux manières pour acceder au dernier jour")
    dernier_jours=jours[-1]
    last_day=jours[6]
    print(last_day)
    print(dernier_jours)

    print("\n4-INVERSE")
    inverse= jours[::-1]
    print(inverse)

def exo2():
    hiver=["Decembre","Janvier", "Fevrier"]
    printemps=["Mars" , "Avril", "Mai"]
    ete=["Juin","Juillet", "Aout",]
    automne=[  "Septembre","Octobre", "Novembre"]
    saisons=[hiver, printemps, ete,automne]

    print(f"\n1-{saisons[2]}")
    print(f"\n2-{saisons[1][0]}")
    print(f"\n3-{saisons[1:2]}")
    print(f"\n4-{saisons[:][1]}")

def exo3():
    table=list(range(0, 91,9))
    print(table)

def exo4():
    nbr_paire=len(range(2,10001 ,2))
    print(f"le nombre des nombres pairs entre 1 à 10000 est: {nbr_paire}")

def exo5():
    semaine=['lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche']
    print(f"1- Afficher la liste :\n{semaine}\n2- La valeur de semain[4]:\n{semaine[4]}")
    
    temp= semaine[0]
    semaine[0]=semaine[6]
    semaine[6]=temp
    print(f"3-Echange des valeurs de la première et la dernière case:\n----->la première valeur:{semaine[0]}\n----->la derniere valeur:{semaine[6]}")
    semaine=['lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche']
    print("4-Afficher 12 fois la valeur du dernier élément:")
    for i in range(12):
        print(f"\n{semaine[6]} ")
    
def exo6():
    lstVide=[];lstFlottant=[0.0]*5
    print(f"- Afficher ces listes:{lstVide} {lstFlottant}")
    
    for i in range(0,1001,200):
        lstVide.append(i)
        
    print(f"Les entiers de 0 à 3:{list(range(0,4))}\nLes entiers de 4 à 7: {list(range(4,8))}\nLes entiers de 2 à 8 par pas de 2:{list(range(2,9,2))}")
    
    lstElmnt= list(range(0,6))
    print(f"lstElmnt:{lstElmnt}")
    lstElmnt.extend(lstVide)
    lstElmnt.extend(lstFlottant)
    print(f"lstElmnt après ajout:{lstElmnt}")
