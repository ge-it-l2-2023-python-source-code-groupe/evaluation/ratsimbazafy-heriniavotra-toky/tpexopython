from chapitres.Listes.exo4_10 import*
from tools.console import clear

def Menu_Liste():
    print("1-Jours de la semaine")
    print("2-Saisons")
    print("3-Table de multiplicationpar 9")
    print("4-Nombres pairs")
    print("5-Liste & indice")
    print("6-Liste & range")
    print("7-Quitter")
    
def Continu():
    continue_encore = input("\nAppuyer sur la touche "'ENTRER'" pour quitter le programme :\n ").lower()
    if continue_encore == ' ':
        return False
    return True

def Liste():
    while True:
        Menu_Liste()

        choix_input = input("Entrez votre choix : ")

        if not choix_input:
            clear()
            print("Veuillez entrer quelque chose.")
            continue

        if not choix_input.isdigit():
            clear()
            print("Veuillez entrer un numéro valide.")
            continue

        choix = int(choix_input)

        if choix == 1:
            while True:
                clear()
                print("\n------ Jour de la semaine ------")
                exo1()
                if Continu():
                    break
                
        elif choix == 2:
            clear()
            while True:
                print("\n------ Saisons ------")
                exo2()
                if Continu():
                    break
                
        elif choix == 3:
            clear()
            while True:
                print("\n------ Table de multiplication par 9 ------")
                exo3()
                if Continu():
                    break
                
        elif choix == 4:
            while True:
                clear()
                print("\n------ Nombres pairs ------")
                exo4()
                if Continu():
                    break
                
        elif choix == 5:
            while True:
                clear()
                print("\n------ Liste & indice ------")
                exo5()
                if Continu():
                    break
        elif choix == 6:
            while True:
                clear()
                print("\n------ Liste & range ------")
                exo6()
                if Continu():
                    break
                    
        elif choix == 7:
            clear()
            print("\n------ Au revoir! ------")
            break
        
        else:
            clear()
            print("Choix invalide. Veuillez entrer un numéro valide.")
    clear()

