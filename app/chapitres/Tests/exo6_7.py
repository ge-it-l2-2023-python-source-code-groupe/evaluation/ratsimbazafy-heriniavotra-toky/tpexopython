from core.settings import YOUR_ENV_VARIABLE
import random

def exo1():
    semaine=["lundi","mardi","mercredi","jeudi","vendredi","samedi","dimanche"]
    i=0
    for  i in range(len(semaine)):
        if i>=0 and i<=3:
            print ("Au travail")
        elif i==4 :
            print("Chouette c'est vendredi!!")
        else:
            print("Repos ce weekend")

def exo2():
    brin=["A", "C", "G", "T", "T", "A", "G", "C", "T", "A", "A", "C", "G"]
    for i in range(len(brin)):
        if   brin[i]=="A":
            brin[i]="T"
        elif   brin[i]=="T":
            brin[i]="A"
        elif   brin[i]=="C":
            brin[i]="G"
        elif brin[i]=="G":
            brin[i]="C"       
    print(brin)
    
def exo3():
    liste= [8, 4, 6, 1, 5]
    valeur_min= min(liste)
    print(f"la valeur minimale est {valeur_min}")
    
def exo4():
    AA=["A", "R", "A", "W", "W", "A", "W", "A", "R", "W", "W", "R", "A", "G"]
    count_A=0
    count_R=0
    count_W=0
    count_G=0

    for i in AA:
        if i=="A":
            count_A+=1
        if i=="R":
            count_R+=1
        if i=="W":
            count_W+=1
        if i=="G":
            count_G+=1
    print(f"la frequence de l'alanine est:{count_A} \nla frequence de arginine est:{count_R}\nla frequence de tryptophane est:{count_W} \nla frequence de glycine est:{count_G}")

def exo5():
    notes =[14, 9, 13, 15 , 12]

    print(f"La note maximale est {max(notes)}\nLa note minimale est {min(notes)}")

    moyenne=sum(notes)/len(notes)
    print(f"La moyenne des notes est {moyenne}")
    if moyenne < 10:
        print("Mediocre")
    elif moyenne >=10 and moyenne < 12:
        print("Passable")
    elif moyenne >=12 and moyenne <14:
        print("Assez-bien")
    elif moyenne >=14 and moyenne <16:
        print("Bien")
    elif moyenne >=16 and moyenne <=20:
        print("Très bien")

def exo6():
    nombre=list(range(0,21))
    for i in nombre:
        if nombre[i] % 2==0 and i<=10:
            print(f"les nombres pairs inférieur ou égal à 10 sont: {nombre[i]}")
        elif nombre[i] % 2!=0 and i>10:
            print (f"les nombres impairs supérieurs à 10 sont: {nombre[i]}")

def exo7():
    def syracuse_sequence(n, iterations=100):
        sequence = [n]
        
        for _ in range(iterations):
            if n == 1:
                break
            elif n % 2 == 0:
                n //= 2
            else:
                n = 3 * n + 1
            sequence.append(n)
        
        return sequence

    def trouverTrivial(sequence):
        cycle_start = sequence.index(1)
        return sequence[cycle_start:]

    num_str = input("Entrez un nombre entier positif pour démarrer la suite de Syracuse : ")

    if num_str.isdigit():
        num = int(num_str)
        if num > 0:
            sequence = syracuse_sequence(num)
            trivial_cycle = trouverTrivial(sequence)
            print(f"Suite de Syracuse pour n={num}: {sequence}")
            print(f"Cycle trivial: {trivial_cycle}")
        else:
            print("Veuillez entrer un nombre entier positif.")
    else:
        print("Veuillez entrer un nombre entier positif valide.")

def exo8():
    angles_phi_psi = [
    [48.6, 53.4],[-124.9, 156.7],[-66.2, -30.8],
    [-58.8, -43.1],[-73.9, -40.6],[-53.7, -37.5],
    [-80.6, -26.0],[-68.5, 135.0],[-64.9, -23.5],
    [-66.9, -45.5],[-69.6, -41.0],[-62.7, -37.5],
    [-68.2, -38.3],[-61.2, -49.1],[-59.7, -41.1]
    ]

    for i, angles in enumerate(angles_phi_psi, start=1):
        phi, psi = angles
        if -57 - 30 <= phi <= -57 + 30 and -47 - 30 <= psi <= -47 + 30:
            print(f"Acide aminé {i}: [{phi}, {psi}] est en hélice.")
        else:
            print(f"Acide aminé {i}: [{phi}, {psi}] n'est pas en hélice.")
            
def exo9():
    print("\nPremière méthode")
    for nombre in range(2, 100):
        est_premier = True

        for i in range(2, int(nombre ** 0.5) + 1):
            if nombre % i == 0:
                est_premier = False
                break

        if est_premier:
            print(nombre, end=' ')

    print()
    
    print("\nDeuxième méthode")

    nombres_premiers = [2]

    for nombre in range(3, 101):
        est_compose = False

        for premier in nombres_premiers:
            if nombre % premier == 0:
                est_compose = True
                break
            
        if est_compose:
            print(f"{nombre} est composé.")
        else:
            
            nombres_premiers.append(nombre)

    print("Liste des nombres premiers jusqu'à 100:", nombres_premiers)
    
    
def exo10():
    
    print("Pensez à un nombre entre 1 et 100.")

    valeur_min, max = 1, 100
    nombre_trouve = False
    nombre_questions = 0

    while not nombre_trouve:
        
        devine = (valeur_min + max) // 2

        print(f"Est-ce que votre nombre est plus grand, plus petit ou égal à {devine} ? [+ / - / =] ", end="")
        reponse = input()

        if reponse == '+':
            valeur_min = devine + 1
        elif reponse == '-':
            max = devine - 1
        elif reponse == '=':
            nombre_trouve = True
        else:
            print("Veuillez répondre avec '+', '-' ou '='.")

        nombre_questions += 1

    print(f"J'ai trouvé en {nombre_questions} questions !")

    
    
    
    
    