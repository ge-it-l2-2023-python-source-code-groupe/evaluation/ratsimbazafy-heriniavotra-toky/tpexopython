from chapitres.Tests.exo6_7 import*
from tools.console import clear


def Menu_Test():
    print("1-Jours de la semaine")
    print("2-Séquence complémentaire d'un brin d'ADN")
    print("3-Minimum d'une liste")
    print("4-Fréquence des acides aminés")
    print("5-Notes et mention d'un étudiant")
    print("6-Nombres pairs")
    print("7-Conjecture de Syracuse")
    print("8-Attribution de la structure secondaire des acides aminés d'une protéine")
    print("9-Détermination des nombres premiers inférieurs à 100")
    print("10-Recherche d'un nombre par dichotomie")   
    print("11-Quitter ")
    
def Continu():
    continue_encore = input("\nAppuyer sur la touche "'ENTRER'" pour quitter le programme :\n ").lower()
    if continue_encore == ' ':
        return False
    return True

def Test():
    while True:
        Menu_Test()

        choix_input = input("Entrez votre choix : ")

        if not choix_input:
            clear()
            print("Veuillez entrer quelque chose.")
            continue

        if not choix_input.isdigit():
            clear()
            print("Veuillez entrer un numéro valide.")
            continue

        choix = int(choix_input)

        if choix == 1:
            clear()
            while True:
                print("\n------ Jours de la semaine ------")
                exo1()
                if Continu():
                    clear()
                    break
                
        elif choix == 2:
            clear()
            while True:
                print("\n------ Séquence complémentaire d'un brin d'ADN ------")
                exo2()
                if Continu():
                    clear()
                    break
                
        elif choix == 3:
            clear()
            while True:
                print("\n------ Minimum d'une liste ------")
                exo3()
                if Continu():
                    clear()
                    break
                
        elif choix == 4:
            clear()
            while True:
                print("\n------ Fréquence des acides aminés ------")
                exo4()
                if Continu():
                    clear()
                    break
                
        elif choix == 5:
            clear()
            while True:
                print("\n------ Notes et mention d'un étudiant ------")
                exo5()
                if Continu():
                    clear()
                    break
        elif choix == 6:
            clear()
            while True:
                print("\n------ Nombres pairs ------")
                exo6()
                if Continu():
                    break
        elif choix == 7:
            clear()
            while True:
                print("\n------ Conjecture de Syracuse ------")
                exo7()
                if Continu():
                    clear()
                    break    
                
        elif choix == 8:
            clear()
            while True:
                print("\n------ Attribution de la structure secondaire des acides aminés d'une protéine ------")
                exo8()
                if Continu():
                    clear()
                    break
                
        elif choix == 9:
            clear()
            while True:
                print("\n------ Détermination des nombres premiers inférieurs à 100 ------")
                exo9()
                if Continu():
                    clear()
                    break            
                    
        elif choix == 10:
            clear()
            while True:
                print("\n------ Recherche d'un nombre par dichotomie ------")
                exo10()
                if Continu():
                    clear()
                    break    
    
        elif choix == 11:
            print("\n------ Au revoir! ------")
            clear()
            break
        
        else:
            clear()
            print("Choix invalide. Veuillez entrer un numéro valide.")
    clear()


