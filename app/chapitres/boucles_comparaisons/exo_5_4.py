from core.settings import YOUR_ENV_VARIABLE
import random

def exo1():
    liste= ["vache", "souris", "levure", "bacterie"]
    for element in liste:
        print(element)
    i=0
    while i < len(liste):
        print(liste[i])
        i += 1 

def exo2():
    semaine=["lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi", "dimanche"]
    for day in semaine:
        print(day)
    
    i=0
    while i <len(semaine):
        if i==5 or i==6:
            print(semaine[i])
        i += 1 

def exo3():
    print(','.join(str(i) for i in range(1, 11)))
    
def exo4():
    impairs=[1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21]
    i=0
    for i in range(len(impairs)):
        impairs[i] += 1
    print(f"liste pairs à partir de la liste impairs:{impairs}")

def exo5():
    notes=[14, 9, 6, 8, 12]
    som=0
    i=0
    for i in range(len(notes)):
        som=som+notes[i]
        i+=1
    moyenne=som/len(notes)
    print(f"La moyenne est: {moyenne:.2f}")

def exo6():
    entiers=list(range(2, 20,2))
    print(entiers)

    i=0
    produit=1
    for i in range(len(entiers)):
        produit= entiers[i]*(entiers[i]+2)
        i+=1 
        print(f"{produit}")

def exo7():
    rows=10
    for i in range(1,rows):
        print("*" *i)
 
def exo8():
    ligne=10
    for i in range(ligne):
        print("*" *(ligne-i))
     
def exo9():
    ligne=10
    for i in range(ligne):
        print(" " *(ligne-i)+"*"*i )

def exo10():
    hauteur_str = input("Entrer un nombre : ")
    hauteur = int(hauteur_str)

    for i in range(1, hauteur + 1):
        espaces = hauteur - i
        etoiles = 2 * i - 1
        ligne = ' ' * espaces + '*' * etoiles
        print(ligne)

def exo11():
    nombre_str = input("Entrer un nombre : ")
    nombre = int(nombre_str)

    for i in range(1, nombre+ 1):
        for j in range(1, nombre + 1):
            print(f"{i} {j}")

def exo12():
    N_str = input("Entrer un nombre : ")

    N = int(N_str)

    total_cases = 0
    for i in range(1, N + 1):
        for j in range(1, N + 1):
            if i != j:
                print(f"{i} {j}")
                total_cases += 1
    print(f"Taille de la matrice : {N} x {N}")
    print(f"Nombre total de cases parcourues : {total_cases}")

def exo13():

    position_initial = 0
    position_finale = 5
    position = 0
    nombre_sauts = 0

    while position != position_finale:
        saut = random.choice([-1, 1])
        position += saut
        nombre_sauts += 1
        print(f"Le nombre de sauts effectués est {nombre_sauts}, position actuelle : {position}")

    print(f"Vous avez atteint la position finale en {nombre_sauts} sauts.")
        
def exo14():
    a= 1
    b=1
    i=0 
    print(a)
    while i<15:
        temp=a+b
        a=b
        b=temp
        i+=1
        print(a)
    