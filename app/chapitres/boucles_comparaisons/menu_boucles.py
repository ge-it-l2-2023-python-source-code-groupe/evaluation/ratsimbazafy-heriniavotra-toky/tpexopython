from chapitres.boucles_comparaisons.exo_5_4 import*
from tools.console import clear

def Menu_boucles():
    print("1-Boucles de bases")
    print("2-Boucle et jours de la semaine")
    print("3-Nombres de 1 à 10")
    print("4-Nombres pairs et impairs")
    print("5-Calcul de la moyenne")
    print("6-Produit de nombres consécutifs")
    print("7-Triangle")
    print("8-Triangle inversé")
    print("9-Tringle à gauche")
    print("10-Pyramide")
    print("11-Parcours Matrice 1")
    print("12-Parcours de demi-matrice sans la diagonale")
    print("13-Sauts de puce")
    print("14-Suite de Fibonacci")
    print("15-Quitter ")
    
def Continu():
    continue_encore = input("\nAppuyer sur la touche "'ENTRER'" pour quitter le programme :\n ").lower()
    if continue_encore == ' ':
        return False
    return True

def Boucle_Comparaison():
    while True:
        Menu_boucles()

        choix_input = input("Entrez votre choix : ")

        if not choix_input:
            clear()
            print("Veuillez entrer quelque chose.")
            continue
        
        if not choix_input.isdigit():
            clear()
            print("Veuillez entrer un numéro valide.")
            continue
            
        
        choix = int(choix_input)

        if choix == 1:
            while True:
                print("\n------ Boucles de bases ------")
                exo1()
                if Continu():
                    break
                
        elif choix == 2:
            while True:
                print("\n------ Boucle et jours de la semaine ------")
                exo2()
                if Continu():
                    break
                
        elif choix == 3:
            while True:
                print("\n------ Nombres de 1 à 10 ------")
                exo3()
                if Continu():
                    break
                
        elif choix == 4:
            while True:
                print("\n------ Nombres pairs et impairs ------")
                exo4()
                if Continu():
                    break
                
        elif choix == 5:
            while True:
                print("\n------ Calcul de la moyenne ------")
                exo5()
                if Continu():
                    break
        elif choix == 6:
            while True:
                print("\n------ Produit de nombres consécutifs ------")
                exo6()
                if Continu():
                    break
        elif choix == 7:
            while True:
                print("\n------ Triangle ------")
                exo7()
                if Continu():
                    break    
                
        elif choix == 8:
            while True:
                print("\n------ Triangle inversé ------")
                exo8()
                if Continu():
                    break
                
        elif choix == 9:
                while True:
                    print("\n------ Tringle à gauche ------")
                    exo9()
                    if Continu():
                        break            
                    
        elif choix == 10:
            while True:
                print("\n------ Pyramide ------")
                exo10()
                if Continu():
                    break    
        
        elif choix == 11:
            while True:
                print("\n------Parcours Matrice ------")
                exo11()
                if Continu():
                    break    
                
        elif choix == 12:
            while True:
                print("\n------ Parcours de Demi-Matrice sans diagonale ------")
                exo12()
                if Continu():
                    break
                
        elif choix == 13:
            while True:
                print("\n------ Sauts de puce ------")
                exo13()
                if Continu():
                    break  
                   
        elif choix == 14:
            while True:
                print("\n------ Suites de Fibonacci ------")
                exo14()
                if Continu():
                    break      
        elif choix == 15:
            print("\n------ Au revoir! ------")
            break
        
        else:
            print("Choix invalide. Veuillez entrer un numéro valide.")
    clear()
