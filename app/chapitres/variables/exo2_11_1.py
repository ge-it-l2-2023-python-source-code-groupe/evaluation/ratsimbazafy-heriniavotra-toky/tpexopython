from core.settings import YOUR_ENV_VARIABLE

def nombre_Friedman():
    print(f"7 + 3\u2076 = {7+3**6} est un nombre de Friedman")
    print(f"(3 + 4)\u2077 = {(3+4)**3} est un nombre de Friedman")
    print(f"3\u2076 − 5 = {3**6-5} n' est pas un nombre de Friedman")
    print(f"(1 + 2\u2078) × 5 = {(1+2**8)*5} est un nombre de Friedman")
    print(f"(2 + 1\u2078)\u2077 = {(2+1**8)**7} est un nombre de Friedman")

