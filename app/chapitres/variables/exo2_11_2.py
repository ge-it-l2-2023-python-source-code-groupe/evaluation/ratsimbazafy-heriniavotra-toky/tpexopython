from core.settings import YOUR_ENV_VARIABLE

def Predire_resultat_operation():
    print(f"(1 +2 )\u2073 = {(1+2)**3}")
    print(f"Da*4 = "+"Da"*4)
    print(f"Da+3 = error")
    print(f"(Pa+La)*2 = "+("Pa"+"La") * 2)
    print(f"(Da*4)/2 = error")
    print(f"5/2 = {5/2}\n5//2 = {5//2}\n5 % 2 = {5%2}")