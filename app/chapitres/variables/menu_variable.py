from chapitres.variables.exo2_11_1 import*
from chapitres.variables.exo2_11_2 import*
from chapitres.variables.exo2_11_3 import*
from tools.console import clear

def Menu_variable():
    print("1-Nombre de Friedman")
    print("2-Opérations")
    print("3-Opérations et conversions de types")
    print("4-Quitter")
    
def Continu():
    continue_encore = input("\nAppuyer sur la touche "'ENTRER'" pour quitter le programme :\n ").lower()
    if continue_encore == ' ':
        return False
    return True

def Variable():
    while True:
        Menu_variable()

        choix_input = input("Entrez votre choix : ")

        if not choix_input:
            clear()
            print("Veuillez entrer quelque chose.")
            continue

        if not choix_input.isdigit():
            clear()
            print("Veuillez entrer un numéro valide.")
            continue

        choix = int(choix_input)

        if choix == 1:
            clear()
            while True:
                print("\n------ Nombre de Friedman ------")
                nombre_Friedman()
                if Continu():
                    clear()
                    break
                
        elif choix == 2:
            clear()
            while True:
                print("\n------ Opérations ------")
                Predire_resultat_operation()
                if Continu():
                    clear()
                    break
                
        elif choix == 3:
            while True:
                clear()
                print("\n------ Opérations et conversions de types ------")
                Predire_resultat_conversion()
                if Continu():
                    clear()
                    break
        
        elif choix == 4:
            clear()
            print("\n------ Au revoir! ------")
            break
        
        else:
            clear()
            print("Choix invalide. Veuillez entrer un numéro valide.")     
    clear()


