from tools.console import clear
from pynput import keyboard
from chapitres.variables.menu_variable import*
from chapitres.Affichage.menu_affichage import*
from chapitres.Listes.menu_liste import*
from chapitres.boucles_comparaisons.menu_boucles import*
from chapitres.Tests.menu_test import*

def on_press(key):
    if key == keyboard.Key.enter:
        return False

def List_Menu():
    print("\n-------Menu principal--------\n")
    print("1-Varibles")
    print("2-Affichage")
    print("3-Liste")
    print("4-Boucles et comparaisons")
    print("5-Tests")
    print("6- Quitter")
    
def Continu():
    continue_encore = input("Voulez-vous rester sur ce programme? (Oui/Entrée(Quitter)): ").lower()
    if continue_encore != 'oui':
        print("------> Programme arreté")
        return False
    return True
    
if __name__ == "__main__":

    while True:
        List_Menu()

        choix_input = input("Entrez votre choix : ")

        if not choix_input:
            print("Veuillez entrer quelque chose.")
            clear()
            continue
            
        if not choix_input.isdigit():
            print("Veuillez entrer un numéro valide.")
            clear()
            continue

        choix = int(choix_input)

        if choix == 1:
            clear()
            while True:
                print("\n------ Varibles ------")
                Variable()
                if not Continu():
                    break
                
        elif choix == 2:
            clear()
            while True:
                print("\n------ Affichage ------")
                Affichage()
                if not Continu():
                    break
        elif choix == 3:
            clear()
            while True:
                print("\n------ Listes ------")
                Liste()
             
                if not Continu(): 
                    break
                 
        elif choix == 4:
            clear()
            while True:
                print("\n------ Boucles et comparaisons ------")
                Boucle_Comparaison()
                if not Continu():
                    break
                
        elif choix == 5:
            clear()
            while True:
                print("\n------ Tests ------")
                Test()
                if not Continu():
                    break
                
        elif choix == 6:
            clear()  
            print("\n------ Au revoir! ------")
            break
            
        else:
            print("Choix invalide. Veuillez entrer un numéro valide.")
            
    clear()  
            
    with keyboard.Listener(on_press=on_press) as listener:
        listener.join()

    clear()
    pass
